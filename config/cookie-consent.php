<?php

return [

    /*
     * Enable the cookie consent dialog.
     */
    'enabled' => env('COOKIE_CONSENT_ENABLED', true),

    /*
     * The cookie name in which we store the boolean
     */
    'cookie_name' => 'site_name_cookie_consent',

    /*
     * Cookie duration in days.
     */
    'cookie_lifetime' => 365 * 20,

    /*
     * Agreement button style.
     */
    'btn_style' => 'btn-green',

    /*
     * Cookie dialog container style.
     */
    'container_color' => '#F2F2F2',
];
