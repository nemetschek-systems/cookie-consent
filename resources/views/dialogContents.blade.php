<div class="container-fluid cookie-container">
    <div class="container">
        <div class="js-cookie-consent cookie-consent">
            <div class="row">
                <div class="col-md-9">
                    <p class="cookie-consent__message text-dark mt-4 mb-4" style="margin-left: -20px;">
                        {!! trans('cookie-consent::texts.message') !!}
                    </p>
                </div>
                <div class="col-md-3 mt-5 flex-shrink-0 w-full">
                    <a class="js-cookie-consent-agree cookie-consent__agree btn {{ $cookieConsentConfig['btn_style'] }} float-right">
                        {{ trans('cookie-consent::texts.agree') }}
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<style>

    .cookie-container {
        position: fixed;
        bottom: 0;
        background: {{ $cookieConsentConfig['container_color'] }};
        z-index: 1000;
    }

    .cookie-consent__agree {
        background: #fff !important;
    }

    @media (max-width: 390px) {
        .cookie-consent__agree.btn {
            float: left !important;
            margin-top: -50px !important;
            margin-left: 0px !important;
            margin-bottom: 25px !important;
        }

        .cookie-consent__message {
            margin-left: 0px !important;
        }
    }

    @media (min-width: 390px) and (max-width: 1199px) {
        .cookie-consent__agree.btn {
            float: left !important;
            margin-top: -50px !important;
            margin-left: 0px !important;
            margin-bottom: 25px !important;
        }

        .cookie-consent__message {
            margin-left: 0px !important;
        }
    }

    @media (min-width: 768px) and (max-width: 1199px) {
        .cookie-consent__agree {
            margin-top: 25px !important;
        }
    }

</style>