<?php

return [
    'message' => 'This website or its third party tools use cookies, which are necessary to its functioning and required to achieve the purposes illustrated in the <strong>cookie policy</strong>.<br>If you want to know more about all or some of the cookies, please refer to the cookie policy.<br>By closing this banner, scrolling this page, clicking a link or continuing to browse otherwise, you agree to the use of cookies.',
    'agree' => 'Allow cookies',
];
