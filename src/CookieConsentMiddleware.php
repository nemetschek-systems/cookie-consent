<?php

namespace NemetschekSystems\CookieConsent;

use Closure;
use Illuminate\Http\Response;

class CookieConsentMiddleware
{
    public function handle($request, Closure $next)
    {
        $response = $next($request);

        if (! config('cookie-consent.enabled')) {
            return $response;
        }

        if (! $response instanceof Response) {
            return $response;
        }

        if (! $this->containsBodyTag($response)) {
            return $response;
        }

        return $this->addCookieConsentScriptToResponse($response);
    }

    /**
     * @param Response $response
     * @return bool
     */
    protected function containsBodyTag(Response $response): bool
    {
        return $this->getLastClosingBodyTagPosition($response->getContent()) !== false;
    }

    /**
     * @param Response $response
     * @return Response
     */
    protected function addCookieConsentScriptToResponse(Response $response): Response
    {
        $content = $response->getContent();

        $closingBodyTagPosition = $this->getLastClosingBodyTagPosition($content);
        $closingBodyTagPosition = $this->getLastClosingBodyTagPosition($content, '<div class="container d-sm-none">');

        $content = ''
            .substr($content, 0, $closingBodyTagPosition)
            .view('cookie-consent::index')->render()
            .substr($content, $closingBodyTagPosition);

        return $response->setContent($content);
    }

    /**
     * @param string $content
     */
    protected function getLastClosingBodyTagPosition(string $content = '', $position = '</header>')
    {
        return strripos($content, $position);
    }
}
