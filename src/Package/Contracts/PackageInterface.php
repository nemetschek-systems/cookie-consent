<?php

namespace NemetschekSystems\CookieConsent\Package\Contracts;

interface PackageInterface
{

    public function setName(string $name): self;

    public function getName(): string;

    public function setNamespace(string $namespace): self;

    public function getNamespace(): string;

    public function setConfigFileName($configFileName = null): self;
}