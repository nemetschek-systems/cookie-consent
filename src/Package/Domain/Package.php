<?php

namespace NemetschekSystems\CookieConsent\Package\Domain;

class Package
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string|null
     */
    public $configFileName = null;

    /**
     * @var bool
     */
    public $hasViews = false;

    /**
     * @var bool
     */
    public $hasConfig = false;

    /**
     * @var string|null
     */
    public $namespace = null;

    /**
     * @var bool
     */
    public $hasTranslations = false;

    /**
     * @var array
     */
    public $viewComposers = [];

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string|null $namespace
     * @return $this
     */
    public function setNamespace(string $namespace = null): self
    {
        $this->namespace = $namespace ?? $this->getName();

        return $this;
    }

    /**
     * @return string
     */
    public function getNamespace(): string
    {
        return $this->namespace;
    }

    /**
     * @param $configFileName
     * @return $this
     */
    public function setConfigFileName($configFileName = null): self
    {
        $this->configFileName = $configFileName ?? $this->getName();

        return $this;
    }

    /**
     * When called, it means that we have added config for the package
     * @return $this
     */
    public function hasConfig(): self
    {
        $this->hasConfig = true;

        $this->setConfigFileName();

        return $this;
    }

    /**
     * When called, it means that we have added views for the package
     * @return $this
     */
    public function hasViews(): self
    {
        $this->hasViews = true;

        return $this;
    }

    /**
     * Deals with partial view files added to a view file
     * https://laravel.com/docs/8.x/views#view-composers
     * @param $view
     * @param $viewComposer
     * @return $this
     */
    public function hasViewComposer($view, $viewComposer): self
    {
        if (! is_array($view)) {
            $view = [$view];
        }

        foreach ($view as $viewName) {
            $this->viewComposers[$viewName] = $viewComposer;
        }

        return $this;
    }

    /**
     * When called, it means that we have added translation files for the package
     * @return $this
     */
    public function hasTranslations(): self
    {
        $this->hasTranslations = true;

        return $this;
    }
}
