<?php

namespace NemetschekSystems\CookieConsent\Package;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use NemetschekSystems\CookieConsent\Package\Domain\Package;
use NemetschekSystems\CookieConsent\Package\Exceptions\InvalidPackage;
use NemetschekSystems\CookieConsent\Package\Traits\Path;

use function config_path;
use function resource_path;

abstract class PackageServiceProvider extends ServiceProvider
{
    use Path;

    /**
     * @var Package
     */
    protected $package;

    /**
     * @param Package $package
     * @return void
     */
    abstract public function configurePackage(Package $package): void;

    /**
     * @return void
     */
    abstract public function packageBooted(): void;

    public function register()
    {
        $this->package = new Package();

        $this->setBasePath($this->getPackageBaseDir());

        $this->configurePackage($this->package);

        if (empty($this->package->name)) {
            throw InvalidPackage::nameIsRequired();
        }

        if ($this->package->hasConfig) {
            $this->mergeConfigFrom(
                $this->basePath("/../config/{$this->package->configFileName}.php"),
                $this->package->configFileName
            );
        }

        return $this;
    }

    public function boot()
    {
        $this->consoleInput();

        if ($this->package->hasTranslations) {
            $this->loadTranslationsFrom(
                $this->basePath('/../resources/lang/'),
                $this->package->getNamespace()
            );

            $this->loadJsonTranslationsFrom($this->basePath('/../resources/lang/'));
            $this->loadJsonTranslationsFrom(resource_path('lang/vendor/' . $this->package->getNamespace()));
        }

        if ($this->package->hasViews) {
            $this->loadViewsFrom($this->basePath('/../resources/views'), $this->package->getNamespace());
        }

        foreach ($this->package->viewComposers as $viewName => $viewComposer) {
            View::composer($viewName, $viewComposer);
        }

        $this->packageBooted();

        return $this;
    }

    /**
     * Checks for console input, description of inputs is provided in the README.md file
     * @return void
     */
    protected function consoleInput(): void
    {
        if ($this->app->runningInConsole()) {
            if ($this->package->hasConfig) {
                $this->publishes([
                    $this->basePath("/../config/{$this->package->configFileName}.php") => config_path(
                        "{$this->package->configFileName}.php"
                    ),
                ], "config");
            }

            if ($this->package->hasViews) {
                $this->publishes([
                    $this->basePath('/../resources/views') => resource_path("views/vendor/{$this->package->getNamespace()}"),
                ], 'views');
            }

            if ($this->package->hasTranslations) {
                $this->publishes([
                    $this->basePath('/../resources/lang') => resource_path("lang/vendor/{$this->package->getNamespace()}"),
                ], "translations");
            }
        }
    }
}
