<?php

namespace NemetschekSystems\CookieConsent\Package\Traits;

use ReflectionClass;

trait Path
{
    /**
     * @var string
     */
    public $basePath;

    /**
     * @param string $path
     * @return $this
     */
    public function setBasePath(string $path): self
    {
        $this->basePath = $path;

        return $this;
    }

    /**
     * @param string|null $directory
     * @return string
     */
    public function basePath(string $directory = null): string
    {
        if ($directory === null) {
            return $this->basePath;
        }

        return $this->basePath . DIRECTORY_SEPARATOR . ltrim($directory, DIRECTORY_SEPARATOR);
    }

    /**
     * Uses the reflection class to get the directory where the class using the trait is located
     * @return string
     */
    public function getPackageBaseDir(): string
    {
        $reflector = new ReflectionClass(get_class($this));

        return dirname($reflector->getFileName());
    }
}