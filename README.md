## Introduction
The package provides an easily configurable view to display the cookie consent dialog message

## Installation
Remove composer.lock file, if present and then execute :

    composer install

## Usage
Add to app/kernel 

    // app/Http/Kernel.php

    class Kernel extends HttpKernel
    {
        protected $middleware = [
            // ...
            \NemetschekSystems\CookieConsent\CookieConsentMiddleware::class,
        ];
    
        // ...
    }

**Customising the texts**

Publish the lang files

    php artisan vendor:publish --provider="NemetschekSystems\CookieConsent\CookieConsentServiceProvider" --tag="translations"

This will publish the files to resources/lang/vendor/cookie-consent

**Customising the views**

Publish the view files

    php artisan vendor:publish --provider="NemetschekSystems\CookieConsent\CookieConsentServiceProvider" --tag="views"


This will publish the files to resources/views/vendor/cookie-consent

**Customising the config**

There are some styles included too.Publish the config file.

    php artisan vendor:publish --provider="NemetschekSystems\CookieConsent\CookieConsentServiceProvider" --tag="config"

This will publish the file to config/cookie-consent.php